cluster_name := "argocd-sandbox"
kubectl_context := "k3d-argocd-sandbox"

init: k3d_init argocd_install dummies_install argocd_print_admin_password

clean: k3d_clean

k3d_init:
    k3d cluster create "{{cluster_name}}"
    # wait for the cluster to be initialized
    sleep 45

k3d_clean:
    k3d cluster delete "{{cluster_name}}"

argocd_repo_add:
    helm repo add argo https://argoproj.github.io/argo-helm

argocd_deps:
    #!/usr/bin/env sh
    cd argocd
    helm dependency build

argocd_install: argocd_repo_add argocd_deps
    helm upgrade --kube-context="{{kubectl_context}}" --install --create-namespace --namespace="argocd" "argocd" ./argocd
    # wait for argocd to be initialized
    sleep 15

argocd_clean:
    helm uninstall --kube-context="{{kubectl_context}}" --namespace="argocd" "argocd"
    # wait for argocd to be deleted
    sleep 15

argocd_serve: argocd_print_admin_password
    # URL: http://0.0.0.0:8100
    # Username: admin
    # Password: `just argocd_print_admin_password`
    kubectl --context="{{kubectl_context}}" port-forward --namespace argocd svc/argocd-server --address 0.0.0.0 8100:80

argocd_print_admin_password:
    kubectl --context="{{kubectl_context}}" get secret -n argocd argocd-initial-admin-secret -o json | jq -r '.data|to_entries|map({key, value:.value|@base64d})|from_entries["password"]'

dummies_install:
    kubectl --context="{{kubectl_context}}" apply -f argocd-apps/dummies.yaml

dummies_clean:
    kubectl --context="{{kubectl_context}}" delete -f argocd-apps/dummies.yaml

dummy_one_serve:
    # URL: http://0.0.0.0:8081
    kubectl --context="{{kubectl_context}}" port-forward --namespace dummy-one svc/yew-homepage-service --address 0.0.0.0 8081:8080

dummy_two_serve:
    # URL: http://0.0.0.0:8082
    kubectl --context="{{kubectl_context}}" port-forward --namespace dummy-two svc/yew-homepage-service --address 0.0.0.0 8082:8080
